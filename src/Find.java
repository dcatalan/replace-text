import java.util.ArrayList;
import java.util.List;

/**
 * Created by dcatalans on 05/05/16.
 */
public class Find {
    private String text;

    public Find(String text) {
        this.text = text;
    }

    public boolean match(String pattern) {
        if (pattern.length() == 0) return false;
        List<Atom> llista = construirLlista(pattern);
        System.out.println(llista);

        for (int i = 0; i < text.length(); i++) {
            if (match2(i, llista) == true)
                return true;
        }

        return false;
    }

    private boolean match2(int i, List<Atom> list) {
        int indexPatro = 0;
        int indexText = i;

        while (indexPatro < list.size()) {

            if (indexText >= text.length()) {
                Atom a = list.get(indexPatro);
                if (a.type == Atom.aType.EOL) {
                    if (indexText == text.length()) return true;
                }
                return false;
            }

            Atom a = list.get(indexPatro);
            char c = text.charAt(indexText);
            switch(a.type) {
                case INTERR:
                    break;
                case CHAR:
                    if (c != a.lletra) return false;
                    break;
                case BOL:
                    if (indexText > 0) return false;
                    indexPatro++;
                    continue;
                case EOL:
                    return false;
                case AC:
                    boolean isAC = false;
                    for (int j = 0; j < a.ar_char.size(); j++) {
                        if(c == a.ar_char.get(j)){
                            isAC = true;
                            break;
                        }
                    }
                    if (!isAC) return false;
                case CLOSURE:
                    if(a.lletra == '*'){
                        continue;
                    }else if(a.lletra == '+'){
                        Atom anterior = list.get(indexPatro-1);
//                        Atom seguent = list.get(indexPatro+1);
                        if (anterior.lletra != c){
                            return false;
                        }
                        break;

                    }

            }
            indexPatro++;
            indexText++;
        }



        return true;
    }

    private List<Atom> construirLlista(String pattern) {
        List<Atom> llista = new ArrayList<Atom>();
        boolean at = false;
        for (int i = 0; i < pattern.length(); i++) {
            char c = pattern.charAt(i);

            if (at) {
                llista.add(createCharAtom(c));
                at = false;
            }
            else {
                if (c == '?') {
                    Atom a = new Atom();
                    a.type = Atom.aType.INTERR;
                    llista.add(a);
                } else if (c == '%') {
                    if(i == 0) {
                        Atom a = new Atom();
                        a.type = Atom.aType.BOL;
                        llista.add(a);
                    }else{
                        llista.add(createCharAtom(c));
                    }

                } else if (c == '$') {
                    if(i != text.length()-1 && i != pattern.length()-1){
                        llista.add(createCharAtom(c));
                    }
                    else {
                        Atom a = new Atom();
                        a.type = Atom.aType.EOL;
                        a.lletra = c;
                        llista.add(a);
                    }
                } else if (c == '@') {
                    at = true;
                    continue;
                }else if(c == '['){
                    i++;
                    Atom a = new Atom();
                    a.type = Atom.aType.AC;
                    List<Character> b = new ArrayList<Character>();

                    while(pattern.charAt(i) != ']') {
                        if(pattern.charAt(i) == '-'){
                            char primer = pattern.charAt(i-1);
                            char segon = pattern.charAt(i+1);

                            for (int j = primer; j <= (int)segon; j++) {
                                a.lletra = pattern.charAt(i);
                                b.add((char)j);
                            }

                        }
                        a.lletra = pattern.charAt(i);
                        b.add(pattern.charAt(i));
                        i++;
                    }
                    a.ar_char = b;
                    llista.add(a);

                } else if(c == '+' || c == '*'){
                    Atom a = new Atom();
                    a.lletra = c;
                    a.type = Atom.aType.CLOSURE;
                    llista.add(a);
                }else {
                    llista.add(createCharAtom(c));
                }
            }
        }
        return llista;
    }
    private Atom createCharAtom(char c){
        Atom a = new Atom();
        a.type = Atom.aType.CHAR;
        a.lletra = c;
        return a;
    }

}

class Atom {
    public enum aType {
        BOL, EOL, CHAR, AC, CLOSURE, INTERR
    }
    aType type;
    char lletra;
    List<Character> ar_char;

    @Override
    public String toString() {
        if(type == aType.CHAR)
            return type.toString() + " " + lletra;
        if(type ==aType.AC)
            return ar_char.toString();

        return "";
    }
}

